const {createApp} = Vue;

createApp({
    data(){
        return{
            show: false,
            itens:["Bola","Bolsa","Tênis"],
            
        };//Fechamento return
    },//Fechamento data
    
    methods:{
        showItens: function(){
            this.show = !this.show;
        },//Fechamento ShowItens
    },//Fechamento methods

}).mount("#app");//Fechamento createApp
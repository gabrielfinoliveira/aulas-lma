const {createApp} = Vue;
createApp({
    data(){
        return{
            paginaInicial:'index.html',
        };
    },

    methods:{
        LoadPage: function(){
            window.location.href = 'pagina2.html'
        },

        VoltarPage: function(){
            window.location.href = 'index.html'
        }
    }
}).mount("#app");

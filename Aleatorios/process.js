const { createApp } = Vue;
createApp({
    data() {
        return {
            randomIndex: 0,
            randomIndexInternet: 0,

            // vetor de imagens locais
            imagensLocais: [
                './Imagens/lua.jpg',
                './Imagens/sol.jpg',
                './Imagens/SENAI_logo.png',
            ],

            imagensInternet:[
                'https://img.freepik.com/vetores-gratis/bela-casa_24877-50819.jpg', 
                'https://mulheresacesta.com.br/wp-content/uploads/2021/02/bola-1.jpg', 
                'https://static.wikia.nocookie.net/onepiece/images/6/64/Roronoa_Zoro_Anime_Pre_Timeskip_Infobox.png/revision/latest?cb=20181209230036&path-prefix=pt',
                'https://conteudo.imguol.com.br/c/esporte/1e/2022/12/06/cristiano-ronaldo-durante-a-partida-entre-portugal-e-suica-pelas-oitavas-de-final-da-copa-do-mundo-do-qatar-1670365258264_v2_450x600.jpg'
            ],
        } //Fim return
    },//Fim data

    computed: {
        randomImage() {
            return this.imagensLocais[this.randomIndex];
        }, // Fim computed

        randomImageInternet()
        {
            return this.imagensInternet[this.randomIndexInternet];
        } // Fim randomInternet
    },

    methods: {
        getRandomImage() {
            this.randomIndex = Math.floor(Math.random() * this.imagensLocais.length);
            this.randomIndexInternet = Math.floor(Math.random() * this.imagensInternet.length);

        }
    }, //Fim methods


}).mount("#app");
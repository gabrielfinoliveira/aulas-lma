//Processamneto dos dados do formulário "index.html"

//Acessando os elemento formulário do html
const formulário = document.getElementById("Formulario1");

//adiciona um ouvinte de eventos a um elemento HTML

formulário.addEventListener("submit", function
(evento)
{
    evento.preventDefault();/previne o comportamento padrão de um elemento HTML em reposta a um evento/

    //constante para tratar dados recebidos dos elementos do formulário
    const nome = document.getElementById("nome").value;
    const email = document.getElementById("email").value;

    //Exibe um alerta com dados coletados 
    //alert(`Nome: ${nome} --- E-mail: ${email}`);

    const updateResultado = document.getElementById("resultado");
    updateResultado.value = `Nome: ${nome} E-mail ${email}`;

    
    updateResultado.style.width = updateResultado.scrollWidth + "px";

});
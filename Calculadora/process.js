const { createApp } = Vue;
createApp({
    data() {
        return {
            display: "0",
            operandoAtual: null,
            operandoAnterior: null,
            operador: null,
        }
    }, //Fechamento da função "data"

    methods:
    {
        handleButtonClick(botao) {
            switch (botao){
                case "+":
                case "-":
                case "÷":
                case "×":

                    this.handleOperation(botao);
                    break;
                

                case "=":
                    this.handleEquals();
                    break;
            
                default:
                    this.handleNumber(botao);
                    break;

            }/*Fim do switch*/
        }, /* fim do andleButtonClick*/

        handleNumber(numero) {

            if (this.display === "0") {
                this.display = numero.toString();
            }
            else {
                this.display += numero.toString();
            }
        }, /*fim do handleNunber*/


        handleOperation(operacao){
            if (this.operandoAtual !== null){
                this.handleEquals();
            } //Fechamento do if
            this.operador = operacao;

            this.operandoAtual = parseFloat(this.display);
            this.display = "0";
        }, //Fechando houndleOpeeration

        handleEquals(){
            const displayAtual = parseFloat(this.display);
            if(this.operandoAtual !== null && this.operador !== null){
                
                switch(this.operador){
                    case "+":
                        this.display = (this.operandoAtual + displayAtual).toString();
                        break;
                        case "-":
                        this.display = (this.operandoAtual - displayAtual).toString();
                        break;
                        case "×":
                        this.display = (this.operandoAtual * displayAtual).toString();
                        break;
                        case "÷":
                        this.display = (this.operandoAtual / displayAtual).toString();
                        break;
                        


                } // Fechamento do switch 

                this.operandoAnterior = this.operandoAtual;
                this.operandoAtual = null;
                this.operador = null;

            } // Fechamento do if
            else
            {
                this.operandoAnterior = displayAtual;
            } // fechamento do else
        }, //Fechamento handleEquals

        

        handleDecimal(){
            if(!this.display.includes(".")){
            {
                this.display += ".";
            }

            }//Fechamento handleDecimal

        }, // Fechamento handleDecimal

        handleClear(){
            this.display = "0";
            this.operandoAtual = null;
            this.operandoAnterior = null;
            this.operador = null;
        }, // Fechamento handleClear  
        
    }, /*Fim do methods*/

}).mount("#app"); //Fechamento do "createApp"
